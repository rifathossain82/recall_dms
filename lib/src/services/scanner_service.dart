import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class ScannerServices{

  static Future scan() async {
    late String scan_result;
    try {
      scan_result = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.BARCODE);
    } on PlatformException {
      scan_result = 'Failed to scan';
    }

    return scan_result;

  }
}
