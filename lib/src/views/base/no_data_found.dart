import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:recall/src/utils/asset_path.dart';
import 'package:recall/src/utils/color.dart';
import 'package:recall/src/utils/styles.dart';
import 'package:recall/src/views/base/helper.dart';
import 'package:recall/src/views/base/k_button.dart';

class NoDataFound extends StatelessWidget {
  final VoidCallback searchAgain;
  const NoDataFound({Key? key, required this.searchAgain}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              AssetPath.noDataFound,
              width: context.height * 0.25,
              height: context.height * 0.25,
              fit: BoxFit.cover,
            ),
            Text(
              'No Data Found!',
              style: h2.copyWith(
                color: kBlackLight,
                fontWeight: FontWeight.w700,
              ),
            ),
            addVerticalSpace(5),
            Text(
              'You have no notifications right now.\nCome back later.',
              textAlign: TextAlign.center,
              style: h5.copyWith(
                color: kGreyTextColor,
              ),
            ),
            addVerticalSpace(16),
            KButton(
              onPressed: searchAgain,
              bgColor: kButtonColor2,
              width: context.width * 0.4,
              borderRadius: 100,
              child: Text(
                'Search Again',
                style: GoogleFonts.roboto(
                  textStyle: h3.copyWith(
                    color: kWhite,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
