import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:recall/src/utils/color.dart';
import 'package:recall/src/utils/dimensions.dart';
import 'package:recall/src/utils/styles.dart';
import 'package:recall/src/views/base/helper.dart';
import 'package:recall/src/views/base/k_button.dart';

class NoNotificationYet extends StatelessWidget {
  const NoNotificationYet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Dimensions.paddingSizeDefault,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/no_notification.jpg',
                width: context.height * 0.25,
                height: context.height * 0.25,
                fit: BoxFit.cover,
              ),
              Text(
                'No Notifications Yet!',
                style: h2.copyWith(
                  color: kBlackLight,
                  fontWeight: FontWeight.w700,
                ),
              ),
              addVerticalSpace(5),
              Text(
                'You have no notifications right now.\nCome back later.',
                textAlign: TextAlign.center,
                style: h5.copyWith(
                  color: kGreyTextColor,
                ),
              ),
              addVerticalSpace(16),
              KButton(
                onPressed: () {},
                bgColor: kButtonColor1,
                width: context.width * 0.4,
                borderRadius: 100,
                child: Text(
                  'Search Again',
                  style: GoogleFonts.roboto(
                    textStyle: h3.copyWith(
                      color: kWhite,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
