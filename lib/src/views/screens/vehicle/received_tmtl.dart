import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:recall/src/controllers/received_tmtl_controller.dart';
import 'package:recall/src/models/vehicle_detail_data.dart';
import 'package:recall/src/services/extensions/build_context_extension.dart';
import 'package:recall/src/services/scanner_service.dart';
import 'package:recall/src/utils/asset_path.dart';
import 'package:recall/src/utils/color.dart';
import 'package:recall/src/utils/dimensions.dart';
import 'package:recall/src/utils/styles.dart';
import 'package:recall/src/views/base/helper.dart';
import 'package:recall/src/views/base/k_appbar.dart';
import 'package:recall/src/views/base/k_button.dart';
import 'package:recall/src/views/base/k_search_text_field.dart';

class ReceivedTMTL extends StatefulWidget {
  RouteData routeData;

  ReceivedTMTL({Key? key, required this.routeData}) : super(key: key);

  @override
  State<ReceivedTMTL> createState() => _ReceivedTMTLState();
}

class _ReceivedTMTLState extends State<ReceivedTMTL> {
  final receivedTMTLController = Get.put(ReceivedTMTLController());
  final TextEditingController searchController = TextEditingController();
  final FocusNode searchFocusNode = FocusNode();
  List<int> selectedTMTL = [];
  List<bool> tmtlValue = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            /// appBar content
            _buildVehicleDetailsAppBar(context),

            /// body content
            Expanded(
              child: Stack(
                children: [
                  _buildVehicleDetailsBody(context),
                  Positioned(
                    bottom: 30,
                    left: 0,
                    right: 0,
                    child: _buildReceivedButton(),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildVehicleDetailsAppBar(BuildContext context) {
    return KAppBar(
      leading: GestureDetector(
        onTap: () {
          Get.delete<ReceivedTMTLController>();
          context.popScreen();
        },
        child: Padding(
          padding: EdgeInsets.all(Dimensions.paddingSizeExtraSmall),
          child: Icon(
            Icons.arrow_back,
            color: kBlackLight,
          ),
        ),
      ),
      title: Text(
        widget.routeData.locationName ?? '',
        style: h2.copyWith(
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  Widget _buildVehicleDetailsBody(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /// search field and scanner section here
          _buildSearchAndScanner(),
          addVerticalSpace(Dimensions.paddingSizeDefault),

          /// tmtl list here
          _buildTMTL(),
          addVerticalSpace(Dimensions.paddingSizeExtraLarge),
        ],
      );

  Widget _buildReceivedButton(){
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
      ),
      child: KButton(
        onPressed: selectedTMTL.isEmpty ? null : (){},
        child: Text(
          'Received',
          style: GoogleFonts.roboto(
            textStyle: h2.copyWith(
              color: kWhite,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSearchAndScanner() {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
      ),
      child: Row(
        children: [
          Expanded(
            child: KSearchTextFiled(
              controller: searchController,
              focusNode: searchFocusNode,
              hintText: 'Search Here',
              inputAction: TextInputAction.search,
            ),
          ),
          addHorizontalSpace(Dimensions.paddingSizeDefault),
          IconButton(
            icon: Icon(
              Icons.qr_code_scanner_sharp,
              color: mainColor,
              size: 30,
            ),
            onPressed: ()async{
              var result = await ScannerServices.scan();
              print(result);
            },
          ),
        ],
      ),
    );
  }

  Widget _buildTMTL() {
    return Expanded(
      child: ListView.separated(
        shrinkWrap: true,
        itemCount: widget.routeData.routeTMTLs!.length,
        itemBuilder: (context, index) {
          tmtlValue.add(false);
          return _buildTMTLBox(
            widget.routeData.routeTMTLs![index],
            index,
          );
        },
        separatorBuilder: (context, index) => Divider(
          color: kDividerColor,
          height: 16,
          thickness: 1.0,
        ),
      ),
    );
  }

  Widget _buildTMTLBox(RouteTMTL tmtl, index) => CheckboxListTile(
        contentPadding: EdgeInsets.symmetric(
          horizontal: Dimensions.paddingSizeDefault,
        ),
        title: Row(
          children: [
            Container(
              height: 49,
              width: 49,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  Dimensions.radiusDefault,
                ),
                color: kActiveColor,
              ),
              child: SvgPicture.asset(
                AssetPath.boxIcon,
                semanticsLabel: 'Box Icon',
              ),
            ),
            addHorizontalSpace(Dimensions.paddingSizeDefault),
            Text(
              'TMTL -${tmtl.id}',
              style: h3.copyWith(
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
        value: tmtlValue[index],
        onChanged: (bool? value) {
          setState(() {
            tmtlValue[index] = value!;
            if (value) {
              selectedTMTL.add(tmtl.id!);
            } else {
              selectedTMTL.removeWhere((element) => element == tmtl.id);
            }
          });
        },
      );
}
