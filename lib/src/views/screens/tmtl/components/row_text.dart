import 'package:flutter/material.dart';
import 'package:recall/src/utils/color.dart';
import 'package:recall/src/utils/dimensions.dart';
import 'package:recall/src/utils/styles.dart';
import 'package:recall/src/views/base/helper.dart';

class RowText extends StatelessWidget {
  final String title;
  final String data;

  const RowText({
    Key? key,
    required this.title,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: h4.copyWith(
            fontWeight: FontWeight.w500,
            color: kGreyTextColor,
          ),
        ),
        addHorizontalSpace(Dimensions.paddingSizeDefault),
        Text(
          data,
          style: h4.copyWith(
            fontWeight: FontWeight.w500,
            color: kGreyTextColor,
          ),
        ),
      ],
    );
  }
}
