import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:recall/src/controllers/tmtl_details_controller.dart';
import 'package:recall/src/models/tmtl_data.dart';
import 'package:recall/src/models/tmtl_detail_data.dart';
import 'package:recall/src/services/extensions/build_context_extension.dart';
import 'package:recall/src/utils/asset_path.dart';
import 'package:recall/src/utils/color.dart';
import 'package:recall/src/utils/dimensions.dart';
import 'package:recall/src/utils/styles.dart';
import 'package:recall/src/views/base/helper.dart';
import 'package:recall/src/views/base/k_appbar.dart';
import 'package:recall/src/views/base/k_text_field.dart';

import 'components/row_text.dart';
import 'components/tmtl_item_card.dart';

class TMTLDetailsScreen extends StatelessWidget {
  final TMTLData tmtl;

  TMTLDetailsScreen({Key? key, required this.tmtl}) : super(key: key);

  final _tmtlDetailsController = Get.put(TMTLDetailsController());
  final TextEditingController _searchController = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  int selected = -1;

  @override
  Widget build(BuildContext context) {
    _tmtlDetailsController.getTMTLDetails(id: tmtl.id!);
    return Scaffold(
      body: SafeArea(
        child: Obx(() {
          return Column(
            children: [
              /// appBar content
              _buildTMTLDetailsAppBar(context),

              /// body content
              Expanded(
                child: _buildTMTLDetailsBody(),
              ),
            ],
          );
        }),
      ),
    );
  }

  Widget _buildTMTLDetailsAppBar(BuildContext context) {
    if (_tmtlDetailsController.isClickSearch.value) {
      _focusNode.requestFocus();
    }
    return KAppBar(
      leading: GestureDetector(
        onTap: () {
          Get.delete<TMTLDetailsController>();
          _searchController.clear();
          context.popScreen();
        },
        child: Padding(
          padding: EdgeInsets.all(Dimensions.paddingSizeExtraSmall),
          child: Icon(
            Icons.arrow_back,
            color: kBlackLight,
          ),
        ),
      ),
      title: _tmtlDetailsController.isClickSearch.value
          ? KTextFiled(
              controller: _searchController,
              focusNode: _focusNode,
              hintText: 'Search here',
              isBorder: false,
              onChanged: (value) {
                print(value);
              },
            )
          : Text(
              'TMTL ${tmtl.id}',
              style: h2.copyWith(
                fontWeight: FontWeight.w600,
              ),
            ),
      actions: [
        GestureDetector(
          onTap: _tmtlDetailsController.changeSearchStatus,
          child: Padding(
            padding: EdgeInsets.all(Dimensions.paddingSizeSmall),
            child: _tmtlDetailsController.isClickSearch.value
                ? Icon(
                    Icons.clear,
                    size: 20,
                    color: mainColor,
                  )
                : SvgPicture.asset(
                    AssetPath.searchIconSvg,
                    semanticsLabel: 'Search Icon',
                  ),
          ),
        ),
      ],
    );
  }

  Widget _buildTMTLDetailsBody() {
    return _tmtlDetailsController.isLoading.value
        ? const Center(child: CircularProgressIndicator())
        : ListView(
            children: [
              _buildItemCard(),
              addVerticalSpace(Dimensions.paddingSizeExtraLarge),
              _buildLocationListTile(),
              Divider(
                color: mainColor,
                height: 20,
                thickness: 0.5,
              ),
              _tmtlDetailsController.tmtlDetails.value.isCurrent == 0
                  ? _buildTMTLItemWithBox()
                  : _buildTMTLItemWithoutBox(),
              addVerticalSpace(Dimensions.paddingSizeExtraLarge),
            ],
          );
  }

  Widget _buildItemCard() => TMTLItemCard(
        tmtlData: tmtl,
        isNavigation: false,
      );

  Widget _buildLocationListTile() => GestureDetector(
        onTap: () async {
          TmtlDetailData tmtlDetails= _tmtlDetailsController.tmtlDetails.value;
          if(tmtlDetails.latitude.toString().isEmpty){
            kSnackBar(message: 'Latitude is missing!', bgColor: failedColor);
          } else if(tmtlDetails.longitude.toString().isEmpty){
            kSnackBar(message: 'Longitude is missing!', bgColor: failedColor);
          } else{
            try{
              double lat = double.parse(tmtlDetails.latitude!);
              double long = double.parse(tmtlDetails.longitude!);
              await openMap(lat, long);
            } catch(e){
              kSnackBar(message: e.toString(), bgColor: failedColor);
            }
          }
        },
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Dimensions.paddingSizeDefault,
          ),
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            leading: Container(
              height: 49,
              width: 49,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
                color: Color(0xFFFFEECC),
              ),
              child: SvgPicture.asset(
                AssetPath.locationIconSvgSvg,
                semanticsLabel: 'Location Icon',
              ),
            ),
            title: Text(
              tmtl.locationName ?? '',
              style: h3.copyWith(
                fontWeight: FontWeight.w500,
              ),
            ),
            subtitle: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  tmtl.assignTime ?? '',
                  style: h4.copyWith(
                    color: mainColor,
                  ),
                ),
                addHorizontalSpace(Dimensions.paddingSizeLarge),
                Text(
                  tmtl.assignDate ?? '',
                  style: h4.copyWith(
                    color: mainColor,
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  /// widgets when is_current = 0
  Widget _buildTMTLItemWithBox() {
    final List tmtlItemList =
        _tmtlDetailsController.tmtlDetails.value.tmtlItems ?? [];
    return ListView.builder(
      itemCount: tmtlItemList.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return _buildBox(index, tmtlItemList[index]);
      },
    );
  }

  Widget _buildBox(int i, TmtlItemsWithBox item) => Column(
        children: [
          Theme(
            data: ThemeData().copyWith(dividerColor: Colors.transparent),
            child: ExpansionTile(
              key: Key(i.toString()),
              initiallyExpanded: i == _tmtlDetailsController.expansionTileIndex,
              title: Text(
                item.boxNo ?? '',
                style: h3.copyWith(
                  fontWeight: FontWeight.w500,
                ),
              ),
              leading: Container(
                height: 49,
                width: 49,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
                  color: mainColor,
                ),
                child: SvgPicture.asset(
                  AssetPath.boxIcon,
                  semanticsLabel: 'Box Icon',
                ),
              ),
              collapsedIconColor: kDarkBgColor,
              iconColor: mainColor,
              children: _buildBoxItem(item.boxItems ?? []),
              childrenPadding: EdgeInsets.only(
                bottom: Dimensions.paddingSizeDefault,
              ),
              onExpansionChanged: ((newState) {
                if (newState) {
                  _tmtlDetailsController.changeExpansionIndex(i);
                } else {
                  _tmtlDetailsController.changeExpansionIndex(-1);
                }
              }),
            ),
          ),
          Divider(
            color: kDividerColor,
            height: 8,
            thickness: 1.0,
          ),
        ],
      );

  _buildBoxItem(List<BoxItems> boxItems) {
    List<Widget> columnContent = [];
    boxItems.forEach((file) => {
          columnContent.add(
            Container(
              padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
              margin: EdgeInsets.only(
                top: Dimensions.marginSizeDefault,
                left: Dimensions.marginSizeDefault,
                right: Dimensions.marginSizeDefault,
              ),
              decoration: BoxDecoration(
                color: kWhite,
                borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(0, 2),
                    blurRadius: 4,
                    spreadRadius: 0,
                    color: kItemShadowColor,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RowText(
                    title: 'Box Code:',
                    data: file.boxCode ?? '',
                  ),
                  RowText(
                    title: 'Batch Code:',
                    data: file.batchCode ?? '',
                  ),
                  RowText(
                    title: 'RN Code:',
                    data: file.rnCode ?? '',
                  ),
                  RowText(
                    title: 'Client File No:',
                    data: file.clientFileNo ?? '',
                  ),
                ],
              ),
            ),
          ),
        });

    return columnContent;
  }

  /// widgets when is_current = 1
  Widget _buildTMTLItemWithoutBox() {
    final List? tmtlItemList =
        _tmtlDetailsController.tmtlDetails.value.tmtlItems ?? [];
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: tmtlItemList!.length,
      itemBuilder: (context, index) => _buildItem(tmtlItemList[index]),
    );
  }

  Widget _buildItem(TmtlItemsWithoutBox item) => Container(
        padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
        margin: EdgeInsets.only(
          top: Dimensions.marginSizeDefault,
          left: Dimensions.marginSizeDefault,
          right: Dimensions.marginSizeDefault,
        ),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 2),
              blurRadius: 4,
              spreadRadius: 0,
              color: kItemShadowColor,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RowText(title: 'Box Code:', data: item.boxCode ?? ''),
            RowText(title: 'Batch Code:', data: item.batchCode ?? ''),
            RowText(title: 'RN Code:', data: item.rnCode ?? ''),
            RowText(title: 'Client File No:', data: item.clientFileNo ?? ''),
          ],
        ),
      );
}
