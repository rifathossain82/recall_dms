/// if the current id of tmtl is 0 it means there are box
/// otherwise there are no box

class TmtlDetailData {
  int? id;
  String? clientName;
  String? locationName;
  String? latitude;
  String? longitude;
  String? address;
  String? assignDate;
  int? isCurrent;
  String? tmtlType;
  int? tmtlItemsCount;
  String? clientShortCode;
  String? assignTime;
  String? type;
  List<dynamic>? tmtlItems;

  TmtlDetailData(
      {this.id,
        this.clientName,
        this.locationName,
        this.latitude,
        this.longitude,
        this.address,
        this.assignDate,
        this.isCurrent,
        this.tmtlType,
        this.tmtlItemsCount,
        this.clientShortCode,
        this.assignTime,
        this.type,
        this.tmtlItems});

  TmtlDetailData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clientName = json['client_name'];
    locationName = json['location_name'];
    latitude = json['latitude'] ?? '';
    longitude = json['longitude'] ?? '';
    address = json['address'] ?? '';
    assignDate = json['assign_date'];
    isCurrent = json['is_current'];
    tmtlType = json['tmtl_type'];
    tmtlItemsCount = json['tmtl_items_count'];
    clientShortCode = json['client_short_code'];
    assignTime = json['assign_time'];
    type = json['type'];
    if(json['is_current'] == 0){
      if (json['tmtlitems'] != null) {
        tmtlItems = <TmtlItemsWithBox>[];
        json['tmtlitems'].forEach((v) {
          tmtlItems!.add(new TmtlItemsWithBox.fromJson(v));
        });
      }
    } else{
      if (json['tmtlitems'] != null) {
        tmtlItems = <TmtlItemsWithoutBox>[];
        json['tmtlitems'].forEach((v) {
          tmtlItems!.add(TmtlItemsWithoutBox.fromJson(v));
        });
      }
    }

  }
}

class TmtlItemsWithoutBox {
  String? boxCode;
  String? batchCode;
  String? rnCode;
  String? clientFileNo;
  String? insertionCode;
  String? insertionRef;

  TmtlItemsWithoutBox(
      {this.boxCode,
        this.batchCode,
        this.rnCode,
        this.clientFileNo,
        this.insertionCode,
        this.insertionRef});

  TmtlItemsWithoutBox.fromJson(Map<String, dynamic> json) {
    boxCode = json['box_code'];
    batchCode = json['batch_code'];
    rnCode = json['rn_code'];
    clientFileNo = json['client_file_no'];
    insertionCode = json['insertion_code'];
    insertionRef = json['insertion_ref'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['box_code'] = this.boxCode;
    data['batch_code'] = this.batchCode;
    data['rn_code'] = this.rnCode;
    data['client_file_no'] = this.clientFileNo;
    data['insertion_code'] = this.insertionCode;
    data['insertion_ref'] = this.insertionRef;
    return data;
  }
}

class TmtlItemsWithBox {
  String? boxNo;
  List<BoxItems>? boxItems;

  TmtlItemsWithBox({this.boxNo, this.boxItems});

  TmtlItemsWithBox.fromJson(Map<String, dynamic> json) {
    boxNo = json['box_no'];
    if (json['box_items'] != null) {
      boxItems = <BoxItems>[];
      json['box_items'].forEach((v) {
        boxItems!.add(new BoxItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['box_no'] = this.boxNo;
    if (this.boxItems != null) {
      data['box_items'] = this.boxItems!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BoxItems {
  String? boxCode;
  String? batchCode;
  String? rnCode;
  String? clientFileNo;

  BoxItems({this.boxCode, this.batchCode, this.rnCode, this.clientFileNo});

  BoxItems.fromJson(Map<String, dynamic> json) {
    boxCode = json['box_code'];
    batchCode = json['batch_code'];
    rnCode = json['rn_code'];
    clientFileNo = json['client_file_no'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['box_code'] = this.boxCode;
    data['batch_code'] = this.batchCode;
    data['rn_code'] = this.rnCode;
    data['client_file_no'] = this.clientFileNo;
    return data;
  }
}