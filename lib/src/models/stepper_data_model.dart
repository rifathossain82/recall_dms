import 'package:flutter/material.dart';

class StepperData {
  final bool isDone;
  final bool isLast;
  final Widget? title;
  final Widget? content;
  final Widget? iconWidget;

  StepperData({
    required this.isDone,
    required this.isLast,
    this.iconWidget,
    this.title,
    this.content,
  });
}
